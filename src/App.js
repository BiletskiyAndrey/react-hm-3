import React, { Component } from 'react';
import './App.css';

//
// const BtnToogle = ({name, isActive, toggle}) => {
//   return (
//       <button className={isActive ? "active" : ""} name={name} onClick={toggle}>Click</button>
//   )
// };
//
// class Toogle extends Component{
//     constructor(props){
//         super(props);
//         this.state = {
//             name: "btn_1"
//         }
//     };
//
//     handlerChange = (event) => {
//         const tar = event.target;
//         const name = tar.name;
//         this.setState({
//             name: name
//         })
//     };
//
//     render(){
//         const children = this.props.children;
//         const curActive = this.state.name;
//         return(
//             <div className="toog-wrapper">
//                 {
//                     React.Children.map(
//                         children,
//                         (ChildrenItem) => {
//                             if (ChildrenItem.props.name === curActive) {
//                                 return React.cloneElement(ChildrenItem, {
//                                     name: ChildrenItem.props.name,
//                                     isActive: true,
//                                     toggle: this.handlerChange
//                                 })
//                             } else {
//                                 return React.cloneElement(ChildrenItem, {
//                                     name: ChildrenItem.props.name,
//                                     toggle: this.handlerChange
//                                 })
//                             }
//                         }
//                     )
//                 }
//             </div>
//         )
//     }
// }
// class App extends Component {
//     render() {
//         return (
//             <div className='app'>
//                 <Toogle>
//                     <BtnToogle name="btn_1"/>
//                     <BtnToogle name="btn_2"/>
//                     <BtnToogle name="btn_3"/>
//                     <BtnToogle name="btn_4"/>
//                 </Toogle>
//                 <br />
//                 <Toogle>
//                     <BtnToogle name="btn_1"/>
//                     <BtnToogle name="btn_2"/>
//                     <BtnToogle name="btn_3"/>
//                     <BtnToogle name="btn_4"/>
//                 </Toogle>
//             </div>
//         );
//     }
// }

//////////////////////////////////////
// const InputComp = ({name, type, value, handler, placeholder}) => {
//     return(
//         <label>
//             <div>{name}</div>
//             <input
//                 name={name}
//                 type={type}
//                 placeholder={placeholder}
//                 value={value}
//                 onChange={handler}
//             />
//         </label>
//     )
// };
//
// const BtnToogle = ({name, value, isActive, toggle}) => {
//     return (
//         <button className={isActive ? "active" : ""} name={name} value={value} onClick={toggle}>Click</button>
//     )
// };
//
// const Toogle = ({children, curActive, handlerChange}) => {
//     return(
//         <div className="toog-wrapper">
//             {
//                 React.Children.map(
//                     children,
//                     (ChildrenItem) => {
//                         if (ChildrenItem.props.value === curActive) {
//                             return React.cloneElement(ChildrenItem, {
//                                 name: ChildrenItem.props.name,
//                                 value: ChildrenItem.props.value,
//                                 isActive: true,
//                                 toggle: handlerChange
//
//                             })
//                         } else {
//                             return React.cloneElement(ChildrenItem, {
//                                 name: ChildrenItem.props.name,
//                                 value: ChildrenItem.props.value,
//                                 toggle: handlerChange
//                             })
//                         }
//                     }
//                 )
//             }
//         </div>
//     )
// };
//
// class App extends Component {
//     constructor(props){
//         super(props);
//         this.state = {
//             name: "btn_1",
//             nameTwo: "btn_1",
//             nameThree: ""
//
//         }
//     };
//
//     handlerChange = (event) => {
//         const tar = event.target;
//         const name = tar.name;
//         const value = tar.value;
//         this.setState({
//             ...this.state,
//             [name]: value
//         })
//     };
//     render() {
//         const {name, nameTwo, nameThree} = this.state;
//         return (
//             <div>
//                 <Toogle curActive={name} handlerChange={this.handlerChange}>
//                     <BtnToogle name="name" value="btn_1"/>
//                     <BtnToogle name="name" value="btn_2"/>
//                     <BtnToogle name="name" value="btn_3"/>
//                     <BtnToogle name="name" value="btn_4"/>
//                 </Toogle>
//
//                 <Toogle curActive={nameTwo} handlerChange={this.handlerChange}>
//                     <BtnToogle name="nameTwo" value="btn_1"/>
//                     <BtnToogle name="nameTwo" value="btn_2"/>
//                     <BtnToogle name="nameTwo" value="btn_3"/>
//                     <BtnToogle name="nameTwo" value="btn_4"/>
//                 </Toogle>
//                 <InputComp name="nameThree" value={nameThree} type="text" placeholder="TEXT" handler={this.handlerChange}/>
//                 <div>{nameThree}</div>
//             </div>
//
//         );
//     }
// }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const Field = ({name, type, value, handler}) => {
    return(
        <label>
            <div>{name}</div>
            <input
                name={name}
                type={type}
                value={value}
                onChange={handler}
            />
        </label>

    )
};

const BtnToggle = ({name,value,ifActive,actToggle,type}) => {
  return (
      <button name={name} value={value} className={ifActive ? "active" : ""} onClick={actToggle} type={type}>{value}</button>
  )
};

const Toggle = ({children, curActive,handler}) => {
  return(
      <div className="wrap">
          {
              React.Children.map(
                  children,
                  (itemChildren) => {
                      if(itemChildren.props.value === curActive){
                          return React.cloneElement(itemChildren, {
                              name:itemChildren.props.name,
                              value:itemChildren.props.value,
                              ifActive:true,
                              actToggle: handler
                          })
                      }else{
                          return React.cloneElement(itemChildren, {
                              name:itemChildren.props.name,
                              value:itemChildren.props.value,
                              actToggle: handler
                          })
                      }
                  }
              )
          }
      </div>
  )
};
class App extends Component{
    constructor(props){
        super(props);
        this.state = {
            inputName:"",
            inputPass:"",
            male:"male",
            inputAge:"",
            layout:"up",
            inputLanguage:""

        }
    }

    changeHandler = (event) => {
        const tar = event.target;
        const name = tar.name;
        const value = tar.value;
        this.setState({
            ...this.state,
            [name]: value
        });
    };

    SentResult = () => {
        const {inputName, inputPass,inputAge,inputLanguage,male,layout} =this.state;
        console.log(inputName, inputPass,inputAge,inputLanguage,male,layout)
    };


    render(){
        const {inputName, inputPass,inputAge,inputLanguage,male,layout} = this.state;
        return(
            <form>
               <Field  name="inputName" value={inputName} type="text" handler={this.changeHandler}/>
               <Field  name="inputPass" value={inputPass} type="password"  handler={this.changeHandler}/>
                <Toggle curActive={male} handler={this.changeHandler}>
                    <BtnToggle name="male" value="male" type="button"/>
                    <BtnToggle name="male" value="famale" type="button"/>
                </Toggle>
                <Field name="inputAge" value={inputAge} type="number" handler={this.changeHandler}/>
                <Toggle curActive={layout} handler={this.changeHandler}>
                    <BtnToggle name="layout" value="up" type="button"/>
                    <BtnToggle name="layout" value="down" type="button"/>
                </Toggle>
                <Field name="inputLanguage" value={inputLanguage} type="text" handler={this.changeHandler}/>
                <Toggle curActive={layout} handler={this.SentResult}>
                    <BtnToggle name="sentBtn" value="sent" type="button"/>
                </Toggle>
            </form>
        )
    }

}
export default App;
